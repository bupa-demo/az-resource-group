azure-terraform-modules

# Introduction 
Azure terraform module to create a Resource Group with optional lock for additional protection.


# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Provider
| Name    | Version |
| :---:   | :---:   |
| azurerm | >=1.35.0|

# Usage

Reference the module as below and pass required and/or optional arguments to provision a resource group. This will create a single module.

```
module "resource_group" {
  source = "git@ssh.dev.azure.com:v3/Delu2011/az-terraform-module/az-resource-group"
  
  rg_name         = var.rg_name
  location        = var.location
  environment     = var.environment
  tags            = var.default_tags
}
```

# Inputs
| Name | Description | Type | Default | Required |
| :---: | :---: | :---: | :---: | :---: |
| rg_name | Name of resource group to be created | string | n/a | Yes |
| location | Azure region to use | string | n/a | yes |
| environment | Environment to deploy | string | n/a | no | 
| project | Name of project | string | n/a | no | 
| lock_level | Lock level to be used for this RG Lock. i.e Empty (no lock), CanNotDelete and ReadOnly. | bool | n/a | no |
| tags | Tags to add to resources | map(string) | n/a | no |


# Outputs
| Name | Description |
| :---: | :---: |
| resource_group_id | Resource group generated id |
| resource_group_location | Resource group location - this is a valid Azure region |
| resource_group_name | Resource group name |