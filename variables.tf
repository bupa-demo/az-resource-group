variable "rg_name" {
  description = "rg name"
  type        = string
}

variable "location" {
  description = "Azure region to use"
  type        = string
}

variable "environment" {
  description = "Project environment"
  type        = string
  default     = ""
}

variable "project" {
  description = "Project name"
  type        = string
  default     = ""
}

variable "lock_level" {
  description = "Specifies the Level to be used for this RG Lock"
  type        = string
  default     = ""
}

variable "tags" {
  description = "Extra tags to add."
  type        = map(string)
  default     = {}
}
